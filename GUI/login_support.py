#! /usr/bin/env python
#  -*- coding: utf-8 -*-
#
# Support module generated by PAGE version 6.0.1
#  in conjunction with Tcl version 8.6
#    Dec 20, 2020 12:34:57 PM CET  platform: Windows NT

import sys
from turtle import rt

from communicatewithapi.GUI import login, lobby, game
from communicatewithapi.GUI import lobby_support
from communicatewithapi.controller import LoginController

try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True

def init(top, gui, *args, **kwargs):
    global w, top_level, root
    w = gui
    top_level = top
    root = top


def LoginButton(p1):
    print('login_support.LoginButton')
    LoginController.onlogin()



    sys.stdout.flush()

def resetButton(p1):
    print('login_support.resetButton')
    sys.stdout.flush()

def destroy_window():
    # Function which closes the window.
    global top_level
    top_level.destroy()
    top_level = None

if __name__ == '__main__':

    login.vp_start_gui()





