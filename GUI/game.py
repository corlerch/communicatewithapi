import random
import tkinter as tk
from tkinter import Canvas, Label, Button, Frame
import requests

#
# Communicate with API
#


import PIL
from PIL import ImageTk, Image
from PIL.ImageTk import PhotoImage

# im = Image.open("cards/BackRed.png")


global card
image_size = (100, 140)


#
# TODO
# emove shown images from the deck
#



SUITS = ['Club', 'Spade', 'Heart', 'Diamond']

cardValues = {'Ace': 11, 'Two': 2, 'Three': 3, 'Four': 4, 'Five': 5, 'Six': 6, 'Seven': 7, 'Eight': 8, 'Nine': 9,
              'Ten': 10, 'Jack': 10, 'Queen': 10,
              'King': 10}



#backgound canvas
back_grid = tk.Canvas( width=800, height=600)
back_grid_img = tk.PhotoImage(file="images/blackjack_bg.png")
back_grid.create_image(0, 0, image=back_grid_img, anchor="nw")
back_grid.pack(expand=True, fill="both")

# first dealer card
v = random.choice(list(cardValues.items()))
s = random.choice(list(SUITS))

player_hand = []
dealer_hand = []
sumP = 0
sumD = 0
global cardc


# Button "Bet"

btn1 = tk.Button(text="Bet", bg="#a34313")
btn1.place_configure(x=250, y=300, width=80, height=50)

global winner

btn2 = tk.Button(text="Hit", bg="#a34313", command = lambda: clickCount(sum))
btn2.place_configure(x=350, y=300, width=80, height=50)
btn3 = tk.Button(text="Stand", bg="#a34313", command= lambda : display(sum))
btn3.place_configure(x=450, y=300, width=80, height=50)


# Button "Hit" -> Dealer's first two cards, player's first cards are shown

def deal():
    global cardc
    print("drawing first covered dealer card ...")
    v1 = random.choice(list(cardValues.items()))
    # removing card from the deck

    s1 = random.choice(list(SUITS))
    print(v1)
    firstdealercardvalue = v1[1]
    #print(v1)
    #print(v1[1])

    # first card front
    cardc = f'{v1[0]}_{s1}.png'
    # first card back

    img = Image.open("cards/BackRed.png")

    image_size = (100, 140)

    card_1 = img.resize(image_size)
    card1 = ImageTk.PhotoImage(card_1)

    img1 = Label(image=card1)
    img1.image = card1
    img1.place_configure(x=200, y=120)

    print("drawing first open dealer card ...")
    v2 = random.choice(list(cardValues.items()))
    s2 = random.choice(list(SUITS))
    firstopendealercardvalue = v2[1]
    print(v2)
    print(v2[1])

    card = f'{v2[0]}_{s2}.png'
    print(card)
    im2_dealer = Image.open(f'cards/{card}')
    image_size = (100, 140)

    card_2 = im2_dealer.resize(image_size)
    card2 = PhotoImage(card_2)

    img2 = Label(image=card2)
    img2.image = card2
    img2.place(x=350, y=120)


    # sum player cards so far
    dealer_hand.append(firstdealercardvalue)
    dealer_hand.append(firstopendealercardvalue)
    sumD = dealer_hand[0] + dealer_hand[1]  # one card and the covered back yet

    dealer_sum_label = Label(text=f'Dealer: {sumD}')
    dealer_sum_label.place(x=400, y=50)

    player_sum_label = Label(text=f'Player: {sumP}')
    player_sum_label.place(x=400, y=570)

    sum = [dealer_hand, player_hand]
    # print(dealer_hand)
    return sum

# Player wants another card
def hit1(sum):

    # first player card
    random.shuffle(list(cardValues.items()))
    print("drawing first player card ...")
    vp1 = random.choice(list(cardValues.items()))
    sp1 = random.choice(list(SUITS))

    cardp = f'{vp1[0]}_{sp1}.png'
    print(cardp)
    im1_player = Image.open(f'cards/{cardp}')
    image_size = (100, 140)

    card_p1 = im1_player.resize(image_size)
    cardp1 = PhotoImage(card_p1)

    imgp1 = Label(image=cardp1)
    imgp1.image = cardp1
    imgp1.place(x=200, y=385)

    # print(sumP)
    # one card, one value
    player_hand.append(vp1[1])
    # sum player cards so far
    sumP = player_hand[0]  # just one card yet

    print(f'Dealer hand after first hit {dealer_hand}')

    sumD =  dealer_hand[0] + dealer_hand[1] # one card and one back yet

    # Dealer gets another card when sum < 17
    print(f'Dealer hand {sumD}')
    if sumD > 21:
        print(("Summe > 21, dealer busts ..."))
        display(sum)
    elif sumD == 21:
        print(("Summe equals 21, dealer wins ..."))
        display(sum)
    elif sumD >= 16 and sumD < 21:
       print("Dealer doesn't take another card")
    # 17 > sumD < 21 another card
    else:
        # third card
        print(f' {sumD} < 17 .Third dealer card')
        v3 = random.choice(list(cardValues.items()))
        s3 = random.choice(list(SUITS))

        card = f'{v3[0]}_{s3}.png'
        print(card)
        im2_dealer = Image.open(f'cards/{card}')
        image_size = (100, 140)

        card_2 = im2_dealer.resize(image_size)
        card2 = PhotoImage(card_2)

        img2 = Label(image=card2)
        img2.image = card2
        img2.place(x=500, y=120)

        # sum dealer cards so far
        dealer_hand.append(v3[1])
        print(f'Third dealer card: {v3}')
        sumD = dealer_hand[0] + dealer_hand[1] + dealer_hand[2] # third card when sum < 17
        print(f'Dealer has {sumD} points')

    print(f'Dealer has {sumD} points')

    # label here
    # print(f' Summe Dealer: {dealer_hand[0]} + {dealer_hand[1]}  + {dealer_hand[2]} = {sumD}')

    dealer_sum_label = Label(text=f'Dealer: {sumD}')
    dealer_sum_label.place(x=400, y=50)

    player_sum_label = Label(text=f'Player: {sumP}')
    player_sum_label.place(x=400, y=570)

    # sum dealer cards so far
    sum =(dealer_hand, player_hand)
    return sum

def hit2(sum):

    dealer_hand = sum[0]
    player_hand = sum[1]

    print(f'Dealer hand after second hit {dealer_hand}')

    # sum dealer cards if no third card
    # Dealer gets another card when sum < 16
    sumD = dealer_hand[0] + dealer_hand[1]
    if sumD > 21:
        print("Summe > 21, dealer busts ...")
        display(sum)

    elif sumD == 21:
        print(("Summe equals 21, dealer wins ..."))
        display(sum)
    elif sumD < 17:
        print(f' {sum} < 21. Another dealer card')
        v4 = random.choice(list(cardValues.items()))
        s4 = random.choice(list(SUITS))

        # third card
        card = f'{v[0]}_{s}.png'
        print(card)
        im4_dealer = Image.open(f'cards/{card}')
        image_size = (100, 140)

        card_4 = im4_dealer.resize(image_size)
        card4 = PhotoImage(card_4)

        img4 = Label(image=card4)
        img4.image = card4
        img4.place(x=500, y=120)

        # sum dealer cards so far
        dealer_hand.append(v4[1])
        sumD = dealer_hand[0] + dealer_hand[1] + dealer_hand[2]    # third card when sum < 17
        print(f' Summe Dealer: {dealer_hand[0]} + {dealer_hand[1]}  + {dealer_hand[2]} = {sumD}')


    # second player card
    random.shuffle(list(cardValues.items()))
    print("second player card ...")
    print(player_hand)
    vp2 = random.choice(list(cardValues.items()))
    sp2 = random.choice(list(SUITS))

    cardp2 = f'{vp2[0]}_{sp2}.png'
    print(cardp2)
    im2_player = Image.open(f'cards/{cardp2}')
    image_size = (100, 140)

    card_p2 = im2_player.resize(image_size)
    cardpl2 = PhotoImage(card_p2)

    imgp2 = Label(image=cardpl2)
    imgp2.image = cardpl2
    imgp2.place(x=350, y=385)

    # sum player cards so far
    player_hand.append(vp2[1])
    print(player_hand)
    sumP = player_hand[0] + player_hand[1]  # two cards yet
    print(f' Summe Player after hit 2: {player_hand[0]} + {player_hand[1]}  = {sumP}')

    if len(dealer_hand) == 2:
        sumD = dealer_hand[0] + dealer_hand[1]
    elif len(dealer_hand) == 3:
        sumD = dealer_hand[0] + dealer_hand[1] + dealer_hand[2]
    else:
        sumD = dealer_hand[0] + dealer_hand[1] + dealer_hand[2] + dealer_hand[3]

    dealer_sum_label = Label(text=f'Dealer: {sumD}')
    dealer_sum_label.place(x=400, y=50)

    player_sum_label = Label(text=f'Player: {sumP}')
    player_sum_label.place(x=400, y=570)


    sum = [dealer_hand, player_hand]
    return  sum


def hit3(sum):
    dealer_hand = sum[0]
    player_hand = sum[1]

    if len(dealer_hand) == 2:
        sumD = dealer_hand[0] + dealer_hand[1]
    elif len(dealer_hand) == 3:
        sumD = dealer_hand[0] + dealer_hand[1] + dealer_hand[2]
    else:
        sumD = dealer_hand[0] + dealer_hand[1] + dealer_hand[2] + dealer_hand[3]

    if sumD > 21:
        print("Summe > 21, dealer busts ...")
        display(sum)

    elif sumD == 21:
        print(("Summe equals 21, dealer wins ..."))
        display(sum)
    elif sumD < 17:
        print(f' Dealer wins, {sumD} < 21. ')
        display(sum)


    # dealer gets a new v´car if sum < 17
    # third player card
    random.shuffle(list(cardValues.items()))
    print("second player card ...")
    vp3 = random.choice(list(cardValues.items()))
    sp3 = random.choice(list(SUITS))

    cardp3 = f'{vp3[0]}_{sp3}.png'
    print(cardp3)
    im3_player = Image.open(f'cards/{cardp3}')
    image_size = (100, 140)

    card_p3 = im3_player.resize(image_size)
    cardpl2 = PhotoImage(card_p3)

    imgp3 = Label(image=cardpl2)
    imgp3.image = cardpl2
    imgp3.place(x=500, y=385)

    # sum player cards so far
    player_hand.append(vp3[1])
    sumP = player_hand[0] + player_hand[1]  + player_hand[2] # two cards yet
    print(f' Summe Player third hit: {player_hand[0]} + {player_hand[1]}  + {player_hand[2]}= {sumP}')


    dealer_sum_label = Label(text=f'Dealer: {sumD}')
    dealer_sum_label.place(x=400, y=50)

    player_sum_label = Label(text=f'Player: {sumP}')
    player_sum_label.place(x=400, y=570)

    sum = [sumD, sumP]
    return sum


sum=deal()

global count
count = 0
global stand
stand = 0

count = 0

def clickCount(sum):

    global count  # inform function to use external variable `count`
    count = count + 1

    if count==1:
        print(f' Hit one time, {dealer_hand}')
        sum=hit1(sum)
    elif count==2:
        print(f' Hit two times, {dealer_hand}')
        sum=hit2(sum)
    elif count == 3:
        print(f' Hit three times, {dealer_hand}')
        sum=hit3(sum)

    return sum




def display(sum):
    dealer_hand = sum[0]
    player_hand = sum[1]

    if len(dealer_hand) == 2:
        sumD = dealer_hand[0] + dealer_hand[1]
    elif len(dealer_hand) == 3:
        sumD = dealer_hand[0] + dealer_hand[1] + dealer_hand[2]
    else:
        sumD = dealer_hand[0] + dealer_hand[1] + dealer_hand[2] + dealer_hand[3]

    #open covered card

    global cardc

    imc_dealer = Image.open(f'cards/{cardc}')
    image_size = (100, 140)

    card_c = imc_dealer.resize(image_size)
    cardc = PhotoImage(card_c)

    imgc = Label(image=cardc)
    imgc.image = cardc
    imgc.place(x=200, y=120)

    if (sumD > sumP) and sumD <= 21:
        winner = "Dealer"
        dealer_endsum_label = Label(text=f'Dealer: {winner} wins')
        dealer_endsum_label.place(x=50, y=120)

    elif (sumP > sumD) and sumP <= 21:
        winner = "Player"
        player_endsum_label = Label(text=f'Player: {winner} wins')
        player_endsum_label.place(x=50, y=500)
    else:
        winner = "Nobody"
        player_endsum_label = Label(text=f'Player: {winner} wins')
        player_endsum_label.place(x=50, y=500)



# only for single start
if __name__ == '__main__':
    root = tk.Tk()
    root.mainloop()